# AMI Baker Pipeline
Creates an AMI in order to launch EC2 instances quickly

## Pipeline
```plantuml
@startuml
:Build Binaries;
:Build AMI;
:Deploy to AWS;
@enduml
```

## Required Variables
Set these in the gitlab CI/CD settings panel:
* `AWS_ACCESS_KEY_ID` - Access Key ID. Get from IAM
* `AWS_SECRET_ACCESS_KEY` - AWS Secret access key
* `AWS_DEFAULT_REGION` - Default AWS region
* `BUILD_GROUP` - Security group to assign to the temporary build instance
* `BUILD_IMAGE` - Base image AMI ID to create the image from
* `SUBNET_ID` - Subnet to use both in the AMI build and to deploy the final server into
* `VPC_ID` - ID of the VPC to deploy into