#!/bin/bash

# Make sure our script exits on error
set -e

# Generate a key
yes | ssh-keygen -b 2048 -t rsa -f /tmp/sshkey -q -N "" > /dev/null

# Request our spot instance
export SPOT_REQUEST_ID=`aws ec2 request-spot-instances \
  --block-duration-minutes 60 \
  --launch-specification file://build/launch-spec.json \
  --query SpotInstanceRequests[0].SpotInstanceRequestId \
  --output text`
echo Spot instance request ID is $SPOT_REQUEST_ID, waiting for our instance

# Wait for just under two minutes to get our reservation,
# if we don't have it by then just exit
for i in {0..10}
do
  if [ $i -eq 10 ]
  then
    echo Tried waiting 10 times for a spot instance, just cancelling it
    aws ec2 cancel-spot-instance-requests --spot-instance-request-ids $SPOT_REQUEST_ID > /dev/null
    exit 1
  fi
  export SPOT_INSTANCE_STATUS=`aws ec2 describe-spot-instance-requests \
  --spot-instance-request-ids $SPOT_REQUEST_ID \
  --query SpotInstanceRequests[0].Status.Code \
  --output text`
  echo Spot instance request status: $SPOT_INSTANCE_STATUS
  if [ "$SPOT_INSTANCE_STATUS" = "fulfilled" ]
  then
    break
  fi
  echo Sleeping for a little
  sleep 3
done

# Get the instance ID
export INSTANCE_ID=`aws ec2 describe-spot-instance-requests \
  --spot-instance-request-ids $SPOT_REQUEST_ID \
  --query SpotInstanceRequests[0].InstanceId \
  --output text`

# If the script errors out, make sure we still know how to shut down the build instance
echo $INSTANCE_ID > ./build/build_instance_id
echo Alright, we snagged a bargain with instance $INSTANCE_ID
export INSTANCE_DESCRIPTION=`aws ec2 describe-instances \
  --instance-ids $INSTANCE_ID`
# Pull out our important variables
export INSTANCE_AZ=`echo $INSTANCE_DESCRIPTION | jq -r .Reservations[0].Instances[0].Placement.AvailabilityZone`
export INSTANCE_IP=`echo $INSTANCE_DESCRIPTION | jq -r .Reservations[0].Instances[0].PublicIpAddress`

# Put the key we generated above onto the instance
sleep 5
echo Dropping our key onto the box
# This can fail with an instance not found, even if the describe-instances call above was fine
aws ec2-instance-connect send-ssh-public-key \
  --instance-id $INSTANCE_ID \
  --instance-os-user ec2-user \
  --availability-zone $INSTANCE_AZ \
  --ssh-public-key file:///tmp/sshkey.pub
# Wait for a bit, found this can be a little flaky if you immediately try to connect
sleep 20
echo Copying binary and systemd config to server at $INSTANCE_IP in availability zone $INSTANCE_AZ
# Still trying to figure out why, but sometimes this fails with a connection refused, we should have slept enough
# above that it shouldn't be an issue
scp -o "StrictHostKeyChecking no" -i /tmp/sshkey ./build/hello-world ./hello-world.service ec2-user@$INSTANCE_IP:~/
# Need to drop our key back onto the server so we can run the commands
aws ec2-instance-connect send-ssh-public-key \
  --instance-id $INSTANCE_ID \
  --instance-os-user ec2-user \
  --availability-zone $INSTANCE_AZ \
  --ssh-public-key file:///tmp/sshkey.pub
sleep 20
ssh -o "StrictHostKeyChecking no" \
    -i /tmp/sshkey ec2-user@$INSTANCE_IP \
    "sudo mkdir -p /opt && sudo mv /home/ec2-user/hello-world /opt/hello-world && sudo mv ~/hello-world.service /etc/systemd/system && sudo systemctl enable hello-world.service && sudo systemctl start hello-world.service"
echo Creating image
export AMI_ID=`aws ec2 create-image \
  --instance-id $INSTANCE_ID \
  --name hello-world-$CI_COMMIT_SHORT_SHA \
  --query ImageId \
  --output text`
echo Waiting for $AMI_ID to create
export AMI_STATE=`aws ec2 describe-images \
  --image-ids $AMI_ID \
  --query Images[0].State \
  --output text`
while [ "$AMI_STATE" = "pending" ]
do
  echo Still waiting on that image
  sleep 30
  export AMI_STATE=`aws ec2 describe-images \
    --image-ids $AMI_ID \
    --query Images[0].State \
    --output text`
done
echo Shutting $INSTANCE_ID down
aws ec2 terminate-instances --instance-ids $INSTANCE_ID > /dev/null
if [ "$AMI_STATE" = "failed" ]
then
  echo Failed to create image:
  aws ec2 describe-images --image-ids $AMI_ID --query Images[0].StateReason.Message --output text
  exit 1
fi
echo $AMI_ID > build/ami_id